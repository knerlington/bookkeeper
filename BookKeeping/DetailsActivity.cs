using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Java.IO;
using Android.Graphics;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;

namespace BookKeeping
{
    /// <summary>
    /// DetailsActivity class
    /// Displays detailed info for selected entry found in the database
    /// </summary>
    [Activity(Label = "DetailsActivity")]

    public class DetailsActivity : Activity
    {
        private TextView textView;
        private ImageView imageView;
        private EntryManager manager;
        private File file, dir;
        private Bitmap bitmap; 

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_details);
            // Create your application here
            textView = (TextView)FindViewById(Resource.Id.textView2);
            imageView = (ImageView)FindViewById(Resource.Id.imageView1);
            manager = EntryManager.Instance;
            
            //Reference to image folder
            dir = new File(Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryPictures), "CameraAppDemo");
            
            //Get intent from previous activity
            int i = Intent.GetIntExtra("ID", 0);

            //Update entry info
            textView.Text = "Type:" + "  " +manager.GetEntry(i).Type + "  " + "Amount:" +"  " +manager.GetEntry(i).Amount;
            file = new File(manager.GetEntry(i).FilePath);
            int height = Resources.DisplayMetrics.HeightPixels;
            int width = imageView.Width;
            bitmap = LoadAndResizeBitmap(file.Path, width, height);
            
            imageView.SetImageBitmap(bitmap);
            Log.Verbose("Intent", i.ToString());
            
            
            
            
            

        }
        public Bitmap LoadAndResizeBitmap(string file, int width, int height)
        {
            // First we get the the dimensions of the file on disk
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            BitmapFactory.DecodeFile(file, options);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            int outHeight = options.OutHeight;
            int outWidth = options.OutWidth;
            int inSampleSize = 1;

            if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
                                   ? outHeight / height
                                   : outWidth / width;
            }

            // Now we will load the image and have BitmapFactory resize it for us.
            options.InSampleSize = inSampleSize;
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(file, options);

            return resizedBitmap;
        }
            
            
            
            
                
                
            
            

            

      
    }
}