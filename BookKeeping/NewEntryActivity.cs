using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BookKeeping.Model;
using SQLite;

using Android.Graphics;
using Android.Provider;
using Android.Content.PM;
using Java.IO;

using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;


namespace BookKeeping
{
    /// <summary>
    /// NewEntryActivity class
    /// Lets the user create a new entry with wanted information
    /// </summary>
    [Activity(Label = "NewEntryActivity", MainLauncher= false)]
    public class NewEntryActivity : Activity
    {
        private EntryManager manager;
        private Button createButton;
        private EditText editAmount;
        private Spinner spinnerType, spinnerAccount, spinnerTax; //spinners holding entry types and entry accounts

        //Camera related variables
        private Bitmap bitmap;
        private File file;
        private File dir;
        ImageView imageView;
        
        private string entryType;

        private void Initialize()
        {
            manager = EntryManager.Instance;
            spinnerType = (Spinner)FindViewById(Resource.Id.spinner1);
            spinnerAccount = (Spinner)FindViewById(Resource.Id.spinner2);
            editAmount = (EditText)FindViewById(Resource.Id.edit_amount);
            

            RadioButton rbIncome = (RadioButton)FindViewById(Resource.Id.radioButton_income);
            RadioButton rbExpense = (RadioButton)FindViewById(Resource.Id.radioButton_expense);
            rbExpense.Click += RadioButtonClicked;
            rbIncome.Click += RadioButtonClicked;

            

            //Fill To/from spinner with money accounts
            spinnerType.Adapter = new AccountAdapter(this, manager.ReadIncomeAccounts());
            spinnerAccount.Adapter = new AccountAdapter(this, manager.ReadMoneyAccounts());
            createButton = (Button)FindViewById(Resource.Id.button_create);
            createButton.Click += CreateEntry;
            
            //TaxRate spinner
            spinnerTax = (Spinner)FindViewById(Resource.Id.spinner3);
            spinnerTax.Adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, manager.ReadTaxRates());

            
            
            
        }


        private void CreateEntry(object sender, EventArgs e)
        {
            if(file != null)
                manager.AddEntry(new Entry() { FilePath = file.Path, Amount = int.Parse(editAmount.Text), Account = manager.ReadMoneyAccounts()[spinnerAccount.SelectedItemPosition].Number, Type = this.entryType });
            else
                manager.AddEntry(new Entry() { FilePath = "There is no image associated with this entry", Amount = int.Parse(editAmount.Text), Account = manager.ReadMoneyAccounts()[spinnerAccount.SelectedItemPosition].Number, Type = this.entryType });
        }


            
           
    
        /// <summary>
        /// FillSpinnerWithAccounts()
        /// Depending on which value "entryType" is set to the respective spinner is filled with respective kind of items
        /// </summary>
        private void FillSpinnerWithAccounts()
        {

            if(entryType == "Income")
            {

                spinnerType.Adapter = new AccountAdapter(this, manager.ReadIncomeAccounts());
                
            }else if(entryType == "Expense")
            {

                spinnerType.Adapter = new AccountAdapter(this, manager.ReadExpenseAccounts());
                
            }

           
            
        }

        /// <summary>
        /// RadioButtonClicked()
        /// RadioButton method called when a radio button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioButtonClicked(object sender, EventArgs e)
        {
            View v = (View)sender;
            
            
            switch(v.Id)
            {
                case Resource.Id.radioButton_income:
                    entryType = "Income";
                    System.Console.WriteLine("Entry type: "+entryType);
                    FillSpinnerWithAccounts();
                    break;
                case Resource.Id.radioButton_expense:
                    entryType = "Expense";
                    System.Console.WriteLine("Entry type: " + entryType);
                    FillSpinnerWithAccounts();
                    break;
            }
            
            
            
        }

       
        

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here
            SetContentView(Resource.Layout.activity_newEntry);
            
            //Init
            Initialize();
            
            
            //Camera
            if (IsThereAnAppToTakePictures())
            {
                CreateDirectoryForPictures();

                Button button = FindViewById<Button>(Resource.Id.button_takePicture);
                imageView = FindViewById<ImageView>(Resource.Id.imageView_picture);
                if (bitmap != null)
                {
                    imageView.SetImageBitmap(bitmap);
                    bitmap = null;
                }
                button.Click += TakeAPicture;
            }

            
        }

        private void TakeAPicture(object sender, EventArgs eventArgs)
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);

            file = new File(dir, String.Format("myPhoto_{0}.jpg", Guid.NewGuid()));
            
            intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(file));

            StartActivityForResult(intent, 0);
        }

        private bool IsThereAnAppToTakePictures()
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            IList<ResolveInfo> availableActivities = PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
        }

        private void CreateDirectoryForPictures()
        {
            dir = new File(Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryPictures), "CameraAppDemo");
            if (!dir.Exists())
            {
                dir.Mkdirs();
            }
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            // make it available in the gallery
            Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
            Uri contentUri = Uri.FromFile(file);
            mediaScanIntent.SetData(contentUri);
            SendBroadcast(mediaScanIntent);

            // display in ImageView. We will resize the bitmap to fit the display
            // Loading the full sized image will consume to much memory 
            // and cause the application to crash.
            int height = Resources.DisplayMetrics.HeightPixels;
            int width = imageView.Width;
            bitmap = LoadAndResizeBitmap(file.Path, width, height);
            imageView.SetImageBitmap(bitmap);
        }

        public Bitmap LoadAndResizeBitmap(string file, int width, int height)
        {
            // First we get the the dimensions of the file on disk
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            BitmapFactory.DecodeFile(file, options);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            int outHeight = options.OutHeight;
            int outWidth = options.OutWidth;
            int inSampleSize = 1;

            if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
                                   ? outHeight / height
                                   : outWidth / width;
            }

            // Now we will load the image and have BitmapFactory resize it for us.
            options.InSampleSize = inSampleSize;
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(file, options);

            return resizedBitmap;
        }

      
    }
}

