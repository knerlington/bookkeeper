using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BookKeeping.Model;

using SQLite;

namespace BookKeeping
{
    /// <summary>
    /// EntryManager
    /// Manages all the entries
    /// Works as a singleton
    /// Can only be one instance
    /// Will be instantiated the first time it's being referenced to 
    /// </summary>
    class EntryManager
    {
        private static EntryManager instance;
        private String dbPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        private SQLiteConnection db;

        //Properties
        public static EntryManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EntryManager();
                    return instance;
                }
                return instance;
            }
        }
        

        /// <summary>
        /// EntryManager()
        /// Constructor set as private so it can act as a singleton
        /// Refer to the class variable Instance where you want access
        /// </summary>
        private EntryManager()
        {
            SetupSQLConnection();
        }

        

        /// <summary>
        /// SetupSQLConnection()
        /// Connects to the database and creates a new table of type Entry if
        /// it doesn't already exist.
        /// </summary>
        public void SetupSQLConnection()
        {
            dbPath += "\\entriesDB.db";
            Console.WriteLine("dbPath: " + dbPath);
            db = new SQLiteConnection(dbPath);
            db.CreateTable<Entry>();
            db.CreateTable<TaxRate>();
            db.CreateTable<Account>();
            db.Close();
            InitDefaultTableEntries();
            
            
            

        }

        /// <summary>
        /// InitDefaultTableEntries()
        /// Initializes default accounts % entries in none already exists
        /// </summary>
        private void InitDefaultTableEntries()
        {
            db = new SQLiteConnection(dbPath);
            if (db.Table<Account>().Count() == 0)
            {
                //Income AutoIncremented ID = 1-2
                db.Insert(new Account() { Name = "F�rs�ljning", Number = 3000});
                db.Insert(new Account() { Name = "F�rs�ljning av tj�nster", Number = 3040 });

                //Expense AutoIncremented ID = 3-5
                db.Insert(new Account() { Name = "F�rbrukningsinventarier och f�rbrukningsmaterial", Number = 5400 });
                db.Insert(new Account() { Name = "�vriga egna uttag", Number = 2013 });
                db.Insert(new Account() { Name = "Reklam och PR", Number = 5900 });

                //Money AutoIncremented ID = 6-8
                db.Insert(new Account() { Name = "Kassa", Number = 1910 });
                db.Insert(new Account() { Name = "F�retagskonto", Number = 1930 });
                db.Insert(new Account() { Name = "Egna ins�ttningar", Number = 2018 });

                
            }
            if(db.Table<Entry>().Count() == 0)
            {
                db.Insert(new Entry() { Account = db.Get<Account>(1).Number, Type = "Income" });
            }
            if(db.Table<TaxRate>().Count() == 0)
            {
                db.Insert(new TaxRate() { Vat =  25});
                db.Insert(new TaxRate() { Vat = 6 });
            }

            db.Close();
        }

        //GETTERS
        //Gets entries, taxrates and accounts using specific ID's
        public Entry GetEntry(int ID)
        {
           
           var e = from i in ReadEntries()
                   where i.ID == ID
                   select i;



           return e.ElementAt(e.Count()-1); 
        }


     
        public Account GetAccount(int ID)
        {
            SQLiteConnection db = new SQLiteConnection(dbPath);
            var data = db.Table<Account>().Where(a => a.ID == ID);
            Account acc = (Account)data.First();

            return acc;
        }
        public TaxRate GetTaxRate(int ID)
        {
            SQLiteConnection db = new SQLiteConnection(dbPath);
            var data = db.Table<TaxRate>().Where(a => a.ID == ID);
            TaxRate t = (TaxRate)data.First();

            return t;
        }

        /// <summary>
        /// AddEntry()
        /// Adds entries to the Entry table
        /// </summary>
        /// <param name="e"></param>
        public void AddEntry(Entry e)
        {
            SQLiteConnection db = new SQLiteConnection(dbPath);
            db.Insert(e);
            db.Close();
        }

        /// <summary>
        /// ReadTaxRates()
        /// Reads all tax rates found in db
        /// </summary>
        /// <returns></returns>
        public List<String> ReadTaxRates()
        {
            SQLiteConnection db = new SQLiteConnection(dbPath);
            var data = db.Table<TaxRate>().ToList();
            db.Close();
            List<String> list = new List<string>();
            foreach(var d in data)
            {
                list.Add(d.Vat.ToString()+ "%");
            }
            return list;
        }

        /// <summary>
        /// ReadAccounts()
        /// Reads all accounts found in database
        /// </summary>
        /// <returns></returns>
        public TableQuery<Account> ReadAccounts()
        {
            
            SQLiteConnection db = new SQLiteConnection(dbPath);
            var data = db.Table<Account>();
            db.Close();
            return data;
        }

        /// <summary>
        /// ReadIncomeAccounts()
        /// Returns the income accounts based on their autoincremented ID's
        /// The ID's are know from the order in which they were created
        /// </summary>
        /// <returns></returns>
        public List<Account> ReadIncomeAccounts()
        {
            SQLiteConnection db = new SQLiteConnection(dbPath);
            var data = db.Table<Account>().Where(a => a.ID <= 2).ToList();


            db.Close();


            return data;
        }

        /// <summary>
        /// ReadExpenseAccounts()
        /// Returns the expense accounts based on their autoincremented ID's
        /// The ID's are know from the order in which they were created
        /// </summary>
        /// <returns></returns>
        public List<Account> ReadExpenseAccounts()
        {
         
            SQLiteConnection db = new SQLiteConnection(dbPath);
            var data = db.Table<Account>().Where(a => a.ID >= 3 && a.ID <= 5).ToList();

            db.Close();
            return data;
        }

        /// <summary>
        /// ReadMoneyAccounts()
        /// Returns the money accounts based on their autoincremented ID's
        /// The ID's are know from the order in which they were created
        /// </summary>
        /// <returns></returns>
        public List<Account> ReadMoneyAccounts()
        {
           
            List<Account> accList;
            SQLiteConnection db = new SQLiteConnection(dbPath);
            accList = db.Table<Account>().Where(a => a.ID >= 6 && a.ID <= 8).ToList();

            db.Close();
            return accList;

            
        }
            
            
       /// <summary>
       /// ReadEntries()
       /// Returns all the entries
       /// </summary>
       /// <returns></returns>
        public List<Entry> ReadEntries()
        {
            List<Entry> entryList;
            SQLiteConnection db = new SQLiteConnection(dbPath);
            entryList = db.Table<Entry>().ToList();
            db.Close();

            return entryList;
        }

        public void TryTypeOfVarForQuery()
        {
            SQLiteConnection db = new SQLiteConnection(dbPath);
            var data = db.Table<Entry>().Where(a => a.Amount < 100).OrderBy(a => a.FilePath);
            Console.WriteLine("Type for var: "+data.GetType());
            foreach (var item in data)
            {
                Console.WriteLine(item);
            }
            
                
        }

        
    }
}