using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BookKeeping.Model;

namespace BookKeeping
{
    /// <summary>
    /// DetailsAdapter class
    /// Adapter used for the DetailsActivity class
    /// </summary>
    class DetailsAdapter : BaseAdapter
    {
        private Activity context;

        public List<Entry> EntryList { get; set; }

        public DetailsAdapter(Activity context, List<Entry> list)
        {

            this.context = context;
            EntryList = list;
        }

        public override int Count
        {
            get { return EntryList.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public Entry GetListItem(int pos)
        {
            return EntryList[pos];
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view;
            if (convertView == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.adapter_account, parent, false); //Pass custom view item layout as param
            }
            else
            {
                view = convertView;
            }


            view.FindViewById<TextView>(Resource.Id.name).Text = "Account: " + EntryList[position].Account.ToString() + "  " + "Amount: " + "  " +EntryList[position].Amount.ToString();

            return view;
        }


    }
}