using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BookKeeping.Model;

namespace BookKeeping
{
    /// <summary>
    /// Used to fill account spinners with correct information
    /// </summary>
    class AccountAdapter : BaseAdapter
    {
        private Activity context;

        public List<Account> AccountList { get; set; }

        public AccountAdapter(Activity context, List<Account> list)
        {
            
            this.context = context;
            AccountList = list;
        }

        public override int Count
        {
            get { return AccountList.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public Account GetListItem(int pos)
        {
            return AccountList[pos];
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view;
            if(convertView == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.adapter_account, parent, false ); //Pass custom view item layout as param
            }
            else
            {
                view = convertView;
            }
            

            view.FindViewById<TextView>(Resource.Id.name).Text = AccountList[position].Name;

            return view;
        }

       
    }
}