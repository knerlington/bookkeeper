using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace BookKeeping.Model
{
    /// <summary>
    /// TaxRate class
    /// Added to the Table<TaxRate> via EntryManager
    /// </summary>
    class TaxRate
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public double Vat { get; set; }

        public TaxRate()
        {
            //Init
        }
    }
}