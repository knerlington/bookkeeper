using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace BookKeeping.Model
{
    /// <summary>
    /// Account class
    /// Holds necessary account data
    /// Should be added to Table<Account> via EntryManager
    /// </summary> 
    class Account
    {

        [PrimaryKey, AutoIncrement, Column("_id")]
        public int ID { get; set; }

        public string Name { get; set; }
        
        public int Number { get; set; }

        public Account()
        {
            //Init
            
        }
    }
}