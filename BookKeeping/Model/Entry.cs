using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace BookKeeping
{
    /// <summary>
    /// Entry
    /// Class repsonsible for the data contained in each entry
    /// Should be added to a Table<Entry> via the EntryManager
    /// </summary>
    class Entry 
    {


        [PrimaryKey, AutoIncrement]
        public int ID { get; private set; }
        public int Account { get; set; }
        public string Type { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }

        

        

      

        /// <summary>
        /// Constructor - Default
        /// 
        /// </summary>
        public Entry()
        {
            if (string.IsNullOrWhiteSpace(Type))
            {
                Type = "Income";
            }
        }
        
    }
}