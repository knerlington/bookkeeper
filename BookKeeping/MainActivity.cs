﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;


namespace BookKeeping
{
    /// <summary>
    /// MainActivity
    /// Entry point for all the UI components
    /// </summary>
    [Activity(Label = "BookKeeping", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        
        
        

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            Initialize();

            
            
        }

        /// <summary>
        /// Initialize()
        /// Inits things
        /// </summary>
        private void Initialize()
        {
            Button newEntryButton = (Button)FindViewById(Resource.Id.button_newEntry);
            Button showEntriesButton = (Button)FindViewById(Resource.Id.button_showEntries);
            newEntryButton.Click += NewEntry;
            showEntriesButton.Click += ViewEntries;
            
        }
        /// <summary>
        /// NewEntry
        /// Starts NewEntryActivity
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewEntry(object sender, EventArgs e)
        {
            Intent i = new Intent(this, typeof(NewEntryActivity));
            StartActivity(i);

        }
        /// <summary>
        /// ViewEntries
        /// Starts ViewEntriesActivity
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewEntries(object sender, EventArgs e)
        {
            Intent i = new Intent(this, typeof(EntryListActivity));
            StartActivity(i);

        }
    }
}

