using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace BookKeeping
{
    class ListAdapter : BaseAdapter<Entry>
    {
        //TODO
        private List<Entry> entryList;
        private Activity context;

        public ListAdapter(Activity contex, List<Entry> entryList) : base()
        {
            this.context = contex;
            this.entryList = entryList;
        }

        public override Entry this[int position]
        {
            get { return this.entryList[position]; }
        }

        public override int Count
        {
            get { return entryList.Count(); }
        }

        public override long GetItemId(int position)
        {
            return entryList[position].ID;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView; // re-use an existing view, if one is available
            if (view == null) // otherwise create a new one
            {
                view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);
            }
                
            view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = this.entryList[position].Type;
            return view;
        }
    }
}