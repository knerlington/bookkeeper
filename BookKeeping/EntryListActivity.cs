using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using SQLite;

namespace BookKeeping
{
    [Activity(Label = "EntryListActivity")]
    public class EntryListActivity : ListActivity
    {
        private EntryManager manager = EntryManager.Instance;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here
            string[] list = {"item 1", "item 2","item 2"};
            
            
            //ListAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, list);
            ListAdapter = new ListAdapter(this, manager.ReadEntries());
            //ListView.ItemClick += ItemClick;
            manager.TryTypeOfVarForQuery();

        }

        private void ItemClick(object sender, EventArgs e)
        {

            Intent intent = new Intent(this, typeof(DetailsActivity));
            
            StartActivity(intent);



        }

        protected override void OnListItemClick(ListView l, View v, int position, long id)
        {
            var list = manager.ReadEntries();
            Log.Verbose("item", list[position].ID.ToString());
            
            //Create intent for new activity
            Intent intent = new Intent(this, typeof(DetailsActivity));
            intent.PutExtra("ID", list[position].ID);
            StartActivity(intent);
        }

    }
}